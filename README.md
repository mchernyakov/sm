# Description
## Project
Project tree:
 ```text
├── README.md
├── sm-test.iml // ignore
├── pom.xml
├── src
└── target // ignore
```

#### Requirements
* Java 8
* Maven 3.3.*
* Mac OS or Linux (I have mac)

## Idea
Simple idea: web-server based on SpringBoot which retrieve data from https://www.coindesk.com/api API.

I've decided to add cache inside the application for better performance. 
Cache only for ranged queries.
Key in the cache is a combination of startDate and endDate.
You can set expire time for value in the cache in configuration.

Current value requested by the scheduler (com.sm.test.service.CheckerService). 
You can set the interval time.

You can find list of endpoints
[below](README.md#endpoints).

### Notes
#### Date format
This solution works only with **yyyy-MM-dd** date format.
#### Currency API
https://api.coindesk.com/ API is free but it has some delays.

## Technologies

#### Spring Boot
Well-knowing framework for microservices and other web-application. 
I've decided to use it only for DI and frame for endpoints.

#### TypesafeConfig
[Typesafe Config (Lightbend)](https://github.com/lightbend/config) -- 
my favorite library for configuration. It is very flexible tool, so, I've decided to use it.

#### Caffeine
[Caffeine](https://github.com/ben-manes/caffeine) 
Well-known framework for caching inside your application.

#### Apache Http client
Simple and useful HTTP-client.

## How to use

#### Build

Build jar and run tests:

```bash
mvn clean package 
```
or
```bash
mvn clean package -Dmaven.test.skip=true
```
#### How to run

Run:
```bash
java -jar target/sm-test-1.0.0.jar

```
Run with custom **reference.conf** (this is file with properties for service), 
you have to set path to file ():
```bash
java -jar -Dconfig.file=<path to file>/reference.conf target/sm-test-1.0.0.jar

```
Example:
 ```bash
java -jar -Dconfig.file=build/etc/reference.conf target/sm-test-1.0.0.jar

 ```
or, for example, you can set a parameter using **-D**:
java -jar -Dserver.port=8080 target/sm-test-1.0.0.jar

#### Parameters
Service has some parameters (_reference.conf_) :
```hocon
server {
  spring {
    profiles = [prod] # profiles
  }
}

service {

  cache { # cache
    max-capacity = 100
    expire-time-sec = 600
  }

  scheduler {
    # seconds
    interval-sec = 1
  }
}
```

#### EndPoints

For current:
```text
/rate
```

For range:
```text
/rate/range?startDate=2019-03-10&endDate=2019-03-11
```
with parameters _startDate_ and _endDate_.

#### Requests

##### Current

Request :

  ```text
$ curl 'http://localhost:8080/rate' -i -X GET
```

Response would be something like this:

 ```text
HTTP/1.1 200 OK
Content-Length: 38
Content-Type: application/json;charset=UTF-8

{"rate":1234.5677}

```
##### Range

Request:

```text
$ curl 'http://localhost:8080/rate/range?startDate=2019-03-01&endDate=2019-03-05' -i -X GET
```

Response:
```text
HTTP/1.1 200 OK
Content-Length: 263
Content-Type: application/json;charset=UTF-8

{
  "range": { 
    "start_date": "2019-03-01",
    "end_date": "2019-03-05"
  },
  "rates": [
    {
      "rate": 3850.0718,
      "date": "2019-03-01"
    },
    {
      "rate": 3845.1133,
      "date": "2019-03-02"
    },
    {
      "rate": 3820.1467,
      "date": "2019-03-03"
    },
    {
      "rate": 3732.5732,
      "date": "2019-03-04"
    },
    {
      "rate": 3880.8,
      "date": "2019-03-05"
    }
  ]
}
```