package com.sm.test.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class DateTimeUtil {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private DateTimeUtil() {
    }

    public static boolean validateDate(String date) {
        LocalDate ld = LocalDate.parse(date, FORMATTER);
        if (ld.isAfter(LocalDate.now())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong date. " + date + " is future");
        }
        String result = ld.format(FORMATTER);
        return result.equals(date);
    }

    public static boolean checkOrder(String start, String end) {
        LocalDate st = LocalDate.parse(start, FORMATTER);
        LocalDate ed = LocalDate.parse(end, FORMATTER);
        if (st.isBefore(ed)) {
            return true;
        } else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong order in dates, " + start + " " + end);
    }
}
