package com.sm.test.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class MainConfig {

    private static final Config ROOT_CONFIG;

    static {
        ROOT_CONFIG = ConfigFactory.load();
    }

    @Bean
    @Qualifier("root")
    @Profile("prod")
    public Config config() {
        return ROOT_CONFIG;
    }

    @Bean
    @Qualifier("server")
    public Config serverConfig() {
        return ROOT_CONFIG.getConfig("server");
    }

    @Bean
    @Qualifier("service")
    public Config serviceConfig() {
        return ROOT_CONFIG.getConfig("service");
    }

    public static Config springConfig() {
        return ROOT_CONFIG.getConfig("server").getConfig("spring");
    }

    @Bean
    @Qualifier("scheduler-config")
    public Config dumpScheduler() {
        return ROOT_CONFIG.getConfig("service.scheduler");
    }

}
