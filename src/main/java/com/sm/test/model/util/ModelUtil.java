package com.sm.test.model.util;

import com.sm.test.model.Range;
import com.sm.test.util.DateTimeUtil;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public final class ModelUtil {

    private ModelUtil() {

    }

    public static Range buildRangeRequest(String start, String end) {
        if (commonCheck(start, end)) {
            return new Range()
                    .setStartDate(start)
                    .setEndDate(end);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad dates. " + start + " " + end);
    }

    private static boolean commonCheck(String start, String end) {
        return DateTimeUtil.validateDate(start)
                && DateTimeUtil.validateDate(end)
                && DateTimeUtil.checkOrder(start, end);
    }
}
