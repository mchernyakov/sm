package com.sm.test.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RateData {

    @JsonProperty("rate")
    private Float rate;

    @JsonProperty("date")
    private String date;

    public RateData() {
    }

    public String getDate() {
        return date;
    }

    public RateData setDate(String date) {
        this.date = date;
        return this;
    }

    public Float getRate() {
        return rate;
    }

    public RateData setRate(Float rate) {
        this.rate = rate;
        return this;
    }

    @Override
    public String toString() {
        return "RateData{" +
                "rate=" + rate +
                ", date='" + date + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RateData)) return false;
        RateData rateData = (RateData) o;
        return Objects.equals(rate, rateData.rate) &&
                Objects.equals(date, rateData.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rate, date);
    }
}
