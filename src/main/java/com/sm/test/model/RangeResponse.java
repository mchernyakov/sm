package com.sm.test.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RangeResponse {
    @JsonProperty("rates")
    private List<RateData> rates;

    @JsonProperty("range")
    private Range range;

    public List<RateData> getRates() {
        return rates;
    }

    public RangeResponse setRates(List<RateData> rates) {
        this.rates = rates;
        return this;
    }

    public Range getRange() {
        return range;
    }

    public RangeResponse setRange(Range range) {
        this.range = range;
        return this;
    }

    @Override
    public String toString() {
        return "RangeResponse{" +
                "rates=" + rates +
                ", range=" + range +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RangeResponse)) return false;
        RangeResponse that = (RangeResponse) o;
        return Objects.equals(rates, that.rates) &&
                Objects.equals(range, that.range);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rates, range);
    }
}
