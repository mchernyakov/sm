package com.sm.test.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.sm.test.model.RateData;
import com.typesafe.config.Config;
import org.apache.http.HttpEntity;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.ZoneId;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class CheckerService implements AutoCloseable {
    private static final Logger logger = LoggerFactory.getLogger(CheckerService.class);

    private final DataService dataService;
    private final ObjectMapper mapper;

    private final ScheduledExecutorService scheduledExecutorService;
    private final int intervalSec;// seconds

    private volatile boolean isWorking = false;

    @Autowired
    public CheckerService(@Qualifier("scheduler-config") Config config, DataService dataService, ObjectMapper mapper) {
        this.mapper = mapper;
        this.dataService = dataService;
        this.intervalSec = config.getInt("interval-sec");

        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    @PostConstruct
    public void start() {
        logger.info("Scheduler is started. Parameters: current time zone - {}, interval (seconds) - {}",
                ZoneId.systemDefault(), intervalSec);

        scheduledExecutorService.scheduleWithFixedDelay(task(), 0, intervalSec, TimeUnit.SECONDS);
    }

    private Runnable task() {
        return () -> {
            if (isWorking) { // to avoid several requests at the same time
                return;
            }

            isWorking = true;
            try {
                Optional<RateData> value = getValue();
                value.ifPresent(v -> {
                    dataService.updateLastValue(v);
                    logger.info("Got new value {}", v.getRate());
                });
            } finally {
                isWorking = false;
            }
        };
    }

    private Optional<RateData> getValue() {
        try {
            URIBuilder builder = new URIBuilder("https://api.coindesk.com/v1/bpi/currentprice/USD.json");

            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                HttpGet httpGet = new HttpGet(builder.build());
                return Optional.ofNullable(httpClient.execute(httpGet, responseHandler()));
            }
        } catch (Exception e) {
            logger.error("Error occurred while executing request for last value", e);
            throw new RuntimeException(e);
        }
    }

    private ResponseHandler<RateData> responseHandler() {
        return httpResponse -> {
            int code = httpResponse.getStatusLine().getStatusCode();
            if (code != HttpStatus.OK.value()) {
                logger.warn("Return code is not 200, {}, full response = {}", code, httpResponse.getStatusLine());
                return null;
            }

            HttpEntity entity = httpResponse.getEntity();
            String body = EntityUtils.toString(entity);
            return buildFromJson(body);
        };
    }

    @VisibleForTesting
    RateData buildFromJson(String json) {
        try {
            JsonNode node = mapper.readTree(json);
            Float rate = (float) node.get("bpi").get("USD").get("rate_float").asDouble();
            return new RateData()
                    .setRate(rate);
        } catch (Exception e) {
            logger.error("Error while converting json to RateData, json = {}", json, e);
            throw new RuntimeException(e);
        }
    }

    @PreDestroy
    @Override
    public void close() throws Exception {
        scheduledExecutorService.shutdown();
        if (scheduledExecutorService.awaitTermination(3, TimeUnit.SECONDS)) {
            scheduledExecutorService.shutdownNow();
            scheduledExecutorService.awaitTermination(1, TimeUnit.SECONDS);
        }
    }
}
