package com.sm.test.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.sm.test.model.Range;
import com.sm.test.model.RangeResponse;
import com.sm.test.model.RateData;
import com.typesafe.config.Config;
import org.apache.http.HttpEntity;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
public class DataService {

    private final static Logger logger = LoggerFactory.getLogger(DataService.class);

    private final LoadingCache<Range, RangeResponse> cache;
    private final ObjectMapper mapper;

    private final ReentrantReadWriteLock.WriteLock writeLock;
    private final ReentrantReadWriteLock.ReadLock readLock;

    // field with lastRate
    private RateData lastRate;

    @Autowired
    public DataService(@Qualifier("service") Config serviceConfig, ObjectMapper mapper) {
        this.mapper = mapper;

        int maxCapacity = serviceConfig.getConfig("cache").getInt("max-capacity");
        int expireTimeSec = serviceConfig.getConfig("cache").getInt("expire-time-sec");
        Preconditions.checkArgument(maxCapacity > 0);
        Preconditions.checkArgument(expireTimeSec > 0);

        this.cache = Caffeine.newBuilder()
                .maximumSize(maxCapacity)
                .expireAfterWrite(expireTimeSec, TimeUnit.SECONDS)
                .build(this::getData);

        ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
        this.readLock = reentrantReadWriteLock.readLock();
        this.writeLock = reentrantReadWriteLock.writeLock();
    }

    @Nonnull
    public RateData getCurrent() {
        RateData res;
        readLock.lock();
        try {
            res = lastRate;
        } finally {
            readLock.unlock();
        }

        if (res == null) {
            logger.warn("Current rateData is null");
            throw new IllegalStateException();
        }
        return res;
    }

    public RangeResponse getRangeData(Range range) {
        return cache.get(range);
    }

    public void updateLastValue(@Nonnull RateData rateData) {
        writeLock.lock();
        try {
            this.lastRate = rateData;
        } finally {
            writeLock.unlock();
        }
    }

    @Nullable
    private RangeResponse getData(Range range) {
        try {
            String start = range.getStartDate();
            String end = range.getEndDate();
            URIBuilder builder = new URIBuilder("https://api.coindesk.com/v1/bpi/historical/close.json")
                    .setParameter("start", start)
                    .setParameter("end", end);

            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                HttpGet httpGet = new HttpGet(builder.build());
                return httpClient.execute(httpGet, responseHandler(range));
            }
        } catch (Exception e) {
            logger.error("Error occurred while executing request for range {}", range, e);
            throw new RuntimeException(e);
        }
    }

    private ResponseHandler<RangeResponse> responseHandler(Range range) {
        return httpResponse -> {
            int code = httpResponse.getStatusLine().getStatusCode();
            if (code != HttpStatus.OK.value()) {
                return null;
            }

            HttpEntity entity = httpResponse.getEntity();
            String body = EntityUtils.toString(entity);
            return buildData(range, body);
        };
    }

    @VisibleForTesting
    RangeResponse buildData(Range range, String json) {
        try {
            JsonNode node = mapper.readTree(json).get("bpi");
            List<RateData> rateData = new ArrayList<>();

            Iterator<Map.Entry<String, JsonNode>> iterator = node.fields();
            while (iterator.hasNext()) {
                Map.Entry<String, JsonNode> jsonNode = iterator.next();
                String keyField = jsonNode.getKey();
                Float rate = jsonNode.getValue().floatValue();
                rateData.add(new RateData().setRate(rate).setDate(keyField));
            }
            return new RangeResponse()
                    .setRange(range)
                    .setRates(rateData);
        } catch (Exception e) {
            logger.error("Error while parsing response json {}", json, e);
            throw new RuntimeException(e);
        }
    }

}
