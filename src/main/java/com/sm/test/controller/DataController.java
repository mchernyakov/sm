package com.sm.test.controller;

import com.sm.test.model.Range;
import com.sm.test.model.RangeResponse;
import com.sm.test.model.RateData;
import com.sm.test.model.util.ModelUtil;
import com.sm.test.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class DataController {

    private final DataService dataService;

    @Autowired
    public DataController(DataService dataService) {
        this.dataService = dataService;
    }

    @GetMapping("/rate")
    public RateData getCurrent() {
        return dataService.getCurrent();
    }

    @GetMapping("/rate/range")
    public RangeResponse getHistorical(@Valid @RequestParam(value = "startDate") String startDate,
                                       @Valid @RequestParam(value = "endDate") String endDate) {
        Range range = ModelUtil.buildRangeRequest(startDate, endDate);
        return dataService.getRangeData(range);
    }

}
