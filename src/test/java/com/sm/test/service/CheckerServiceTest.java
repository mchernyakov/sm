package com.sm.test.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sm.test.model.RateData;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CheckerServiceTest {

    private CheckerService checkerService;

    @Before
    public void setUp() throws Exception {
        Config config = ConfigFactory.load();
        checkerService = new CheckerService(config.getConfig("service.scheduler"), null, new ObjectMapper());
    }

    @After
    public void tearDown() throws Exception {
        checkerService.close();
    }

    @Test
    public void buildFromJsonTest() throws Exception {
        String data = "{\n" +
                "  \"time\": {\n" +
                "    \"updated\": \"Mar 16, 2019 11:02:00 UTC\",\n" +
                "    \"updatedISO\": \"2019-03-16T11:02:00+00:00\",\n" +
                "    \"updateduk\": \"Mar 16, 2019 at 11:02 GMT\"\n" +
                "  },\n" +
                "  \"disclaimer\": \"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org\",\n" +
                "  \"bpi\": {\n" +
                "    \"USD\": {\n" +
                "      \"code\": \"USD\",\n" +
                "      \"rate\": \"4,040.6233\",\n" +
                "      \"description\": \"United States Dollar\",\n" +
                "      \"rate_float\": 4040.6233\n" +
                "    }\n" +
                "  }\n" +
                "}";

        RateData rateData = checkerService.buildFromJson(data);
        System.out.println(rateData);
        String json = new ObjectMapper().writeValueAsString(rateData);
        System.out.println(json);
        assertEquals(json, "{\"rate\":4040.6233}");
    }

}