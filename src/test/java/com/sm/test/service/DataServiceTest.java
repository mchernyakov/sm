package com.sm.test.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.sm.test.model.Range;
import com.sm.test.model.RangeResponse;
import com.sm.test.model.RateData;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DataServiceTest {

    private DataService dataService;

    @Before
    public void setUp() throws Exception {
        Config config = ConfigFactory.load();
        dataService = new DataService(config.getConfig("service"), new ObjectMapper());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testConvert() throws Exception {
        String data = "{\n" +
                "  \"bpi\": {\n" +
                "    \"2019-03-01\": 3850.0717,\n" +
                "    \"2019-03-02\": 3845.1133,\n" +
                "    \"2019-03-03\": 3820.1467,\n" +
                "    \"2019-03-04\": 3732.5733,\n" +
                "    \"2019-03-05\": 3880.8\n" +
                "  },\n" +
                "  \"disclaimer\": \"This data was produced from the CoinDesk Bitcoin Price Index. BPI value data returned as USD.\",\n" +
                "  \"time\": {\n" +
                "    \"updated\": \"Mar 6, 2019 00:03:00 UTC\",\n" +
                "    \"updatedISO\": \"2019-03-06T00:03:00+00:00\"\n" +
                "  }\n" +
                "}";

        RangeResponse rangeResponse = dataService.buildData(new Range().setStartDate("2019-03-01").setEndDate("2019-03-05"), data);

        System.out.println(rangeResponse);
        System.out.println(new ObjectMapper().writeValueAsString(rangeResponse));

        RangeResponse exp = new RangeResponse().
                setRange(new Range()
                        .setStartDate("2019-03-01")
                        .setEndDate("2019-03-05"))
                .setRates(Lists.newArrayList(
                        new RateData().setRate(3850.0717f).setDate("2019-03-01"),
                        new RateData().setRate(3845.1133f).setDate("2019-03-02"),
                        new RateData().setRate(3820.1467f).setDate("2019-03-03"),
                        new RateData().setRate(3732.5733f).setDate("2019-03-04"),
                        new RateData().setRate(3880.8f).setDate("2019-03-05")
                ));

        assertEquals(exp, rangeResponse);
    }



}