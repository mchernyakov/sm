package com.sm.test.model.util;

import com.sm.test.model.Range;
import org.junit.Test;
import org.springframework.web.server.ResponseStatusException;

public class ModelUtilTest {

    @Test
    public void testRangeConvert() {
        Range range = ModelUtil.buildRangeRequest("2019-03-08", "2019-03-10");
    }

    @Test(expected = ResponseStatusException.class)
    public void testRangeConvert0() {
        Range range = ModelUtil.buildRangeRequest("2020-03-08", "2019-03-10");
    }

    @Test(expected = ResponseStatusException.class)
    public void testRangeConvert1() {
        Range range = ModelUtil.buildRangeRequest("2019-03-08", "2019-03-07");
    }
}