package com.sm.test.util;

import org.junit.Test;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.Assert.assertTrue;

public class DateTimeUtilTest {

    @Test(expected = ResponseStatusException.class)
    public void testCheck0() throws Exception {
        DateTimeUtil.validateDate("2020-03-14");
    }

    @Test
    public void testCheck1() throws Exception {
        assertTrue(DateTimeUtil.validateDate("2019-03-14"));
    }
}