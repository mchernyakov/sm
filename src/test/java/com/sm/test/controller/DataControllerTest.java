package com.sm.test.controller;

import com.google.common.collect.Lists;
import com.sm.test.model.Range;
import com.sm.test.model.RangeResponse;
import com.sm.test.model.RateData;
import com.sm.test.service.CheckerService;
import com.sm.test.service.DataService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class DataControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DataService dataService;

    @MockBean
    private CheckerService checkerService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.when(dataService.getCurrent()).thenReturn(new RateData().setRate(1234.5678f).setDate("2019-03-15"));
    }

    @Test
    public void getCurrentTest() throws Exception {
        Mockito.when(dataService.getCurrent()).thenReturn(new RateData().setRate(1234.5678f).setDate("2019-03-15"));

        String path = "/rate";
        String respRes = this.mockMvc
                .perform(MockMvcRequestBuilders.get(path))
                .andDo(print())
                .andDo(document("current"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(respRes);
        assertEquals("{\"rate\":1234.5677,\"date\":\"2019-03-15\"}", respRes);
    }

    @Test
    public void getRangeTest() throws Exception {
        Mockito.when(dataService.getRangeData(new Range().setStartDate("2019-03-01").setEndDate("2019-03-05"))).thenReturn(new RangeResponse().
                setRange(new Range()
                        .setStartDate("2019-03-01")
                        .setEndDate("2019-03-05"))
                .setRates(Lists.newArrayList(
                        new RateData().setRate(3850.0717f).setDate("2019-03-01"),
                        new RateData().setRate(3845.1133f).setDate("2019-03-02"),
                        new RateData().setRate(3820.1467f).setDate("2019-03-03"),
                        new RateData().setRate(3732.5733f).setDate("2019-03-04"),
                        new RateData().setRate(3880.8f).setDate("2019-03-05")
                )));

        String path = "/rate/range?startDate=2019-03-01&endDate=2019-03-05";
        String respRes = this.mockMvc
                .perform(MockMvcRequestBuilders.get(path))
                .andDo(print())
                .andDo(document("range"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(respRes);
        String exp = "{\"rates\":[{\"rate\":3850.0718,\"date\":\"2019-03-01\"}," +
                "{\"rate\":3845.1133,\"date\":\"2019-03-02\"},{\"rate\":3820.1467,\"date\":\"2019-03-03\"}," +
                "{\"rate\":3732.5732,\"date\":\"2019-03-04\"},{\"rate\":3880.8,\"date\":\"2019-03-05\"}]," +
                "\"range\":{\"start_date\":\"2019-03-01\",\"end_date\":\"2019-03-05\"}}";
        assertEquals(exp, respRes);
    }

    @Test
    public void getRange400() throws Exception {
        String path = "/rate/range?startDate=2019-05-01&endDate=2019-03-05";
        this.mockMvc
                .perform(MockMvcRequestBuilders.get(path))
                .andDo(print())
                .andDo(document("range-400-0"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @Test
    public void getByIdAnother400() throws Exception {
        String path = "/rate/range?startDate=3020-01-01&endDate=3020-03-05";
        this.mockMvc
                .perform(MockMvcRequestBuilders.get(path))
                .andDo(print())
                .andDo(document("range-400-1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
}